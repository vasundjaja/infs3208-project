from flask import Blueprint, render_template, send_from_directory, request, json, Markup
from flask_login import login_required, current_user
from flask_cors import CORS, cross_origin
import requests
from .models import *
from . import db
from bs4 import BeautifulSoup
from .scraper import bookScrapper, getMovies

#App configuration
main = Blueprint('main', __name__)

cors = CORS(main, resources={r"/view-all/*": {"origins": "*"}})

#Data source
users = [{'uid': 1, 'name': 'Vincentius Aditya'}]

################## BOOKS LIST ##################
books = bookScrapper()
movies = getMovies()

@main.route('/')
def index():
    if current_user.is_authenticated:
        return render_template('index.html', data=current_user)
    else:
        return render_template('index.html')

#Api route to get users
@main.route('/profile')
@login_required
def profile():
    # filter saved books according to user
    books_db = Book.query.filter_by(user_id=current_user.id).filter_by(type="book").all()
    saved_books = []
    for book in books_db:
        a_book = book.__dict__
        a_book['book_rating_elem'] = Markup(a_book['book_rating_elem'])
        saved_books.append(a_book)

    # filter saved movies according to user
    movies_db = Movie.query.filter_by(user_id=current_user.id).filter_by(type="movie").all()
    saved_movies = []
    for movie in movies_db:
        a_movie = movie.__dict__
        saved_movies.append(a_movie)
    
    return render_template("profile.html", data=current_user, saved_books=saved_books, saved_movies=saved_movies)

@main.route('/landing')
@login_required
def landing():
    # print(books)
    # books = bookScrapper()
    mod_books = checkSavedActivity(current_user.id, books, 'book')
    mod_movies = checkSavedActivity(current_user.id, movies, 'movie')
    return render_template("landing.html", data=current_user, books=mod_books, movies=mod_movies)


@main.route('/check-saved-activity', methods=['POST'])
@login_required
def checkSavedActivity():
    movies = request.json['data']
    mod_movies = checkSavedActivity(current_user.id, movies, 'movie')

    return json.dumps({'status':'OK', 'movies':movies})

# function to check if the activities loaded is already saved by the user
def checkSavedActivity(user_id, activities, type_):
    mod_activities = []
    for activity in activities:
        if (type_ == 'book'):
            book_id = activity['book_id']
            book = Book.query.filter_by(user_id=user_id).filter_by(book_id=book_id).first()
            saved = 1 if book else 0

        elif (type_ == 'movie'):
            movie_id = activity['id']
            movie = Movie.query.filter_by(user_id=user_id).filter_by(movie_id=movie_id).first()
            saved = 1 if movie else 0

        activity['saved'] = saved
        mod_activities.append(activity)

    return mod_activities

@main.route('/view-all/<string:activity>')
@cross_origin()
@login_required
def view_all(activity):
    # print(activity)
    # books = bookScrapper()
    mod_books = checkSavedActivity(current_user.id, books, 'book')
    return render_template("view-all.html", data=current_user, title=activity.upper(), books=mod_books)

@main.route('/search-book', methods=['GET', 'POST'])
@login_required
def searchBook():
    URL = request.json['url']
    # print(URL)

    page = requests.get(URL)
    soup = BeautifulSoup(page.content, 'lxml')
    works = soup.find_all("work")

    books = []
    works = works if len(works) <= 50 else works[0:50]
    for work in works:
        avg_rating = work.find("average_rating").text
        ratings_count = work.find("ratings_count").text

        avg_rating_int = int(float(avg_rating)*10)
        p10 = avg_rating_int // 10
        p6 = 0 if (avg_rating_int % 10) < 5 else 1
        p3 = 0 if (avg_rating_int % 10) >= 5 else 1
        p0 = 4 - p10
        staticStar = "<span class='staticStar p10' size='12x12'></span>"*p10 + \
                     "<span class='staticStar p6' size='12x12'></span>"*p6 + \
                     "<span class='staticStar p3' size='12x12'></span>"*p3 + \
                     "<span class='staticStar p0' size='12x12'></span>"*p0

        rating_elem = "<span class='greyText smallText uitext'>\
                        <span class='minirating'>\
                            <span class='stars staticStars notranslate'>\
                                {}\
                            </span>\
                            {} avg rating — {} ratings\
                        </span>\
                      </span>".format(staticStar, avg_rating, ratings_count)
        rating_elem = Markup(rating_elem)

        book_id = work.find("best_book").find("id").text
        title = work.find("best_book").find("title").text
        author = work.find("best_book").find("author").find("name").text
        img_src = work.find("best_book").find("image_url").text
        img_src_small  = work.find("best_book").find("small_image_url").text
        href = "https://www.goodreads.com/book/show/" + book_id

        # print(img_src)
        book = Book.query.filter_by(user_id=current_user.id).filter_by(book_id=book_id).first()
        saved = 1 if book else 0
        print(title, saved)

        a_book = {"book_id": book_id,
                  "img": img_src,
                  "img_small": img_src_small,
                  "title": title,
                  "href": href,
                  "ratingElem": rating_elem,
                  "author": author,
                  "saved": saved}
        books.append(a_book)

    return json.dumps({'status':'OK', 'books':books})

@main.route('/delete-activity', methods=['POST'])
@login_required
def deleteActivity():
    user_id = current_user.id
    type_ = request.json['type']

    if (type_ == 'book'):
        book_id = request.json['book_id']
        Book.query.filter_by(user_id=user_id).filter_by(book_id=book_id).delete()
        db.session.commit()
    elif (type_ == 'movie'):
        movie_id = request.json['movie_id']
        Movie.query.filter_by(user_id=user_id).filter_by(movie_id=movie_id).delete()
        db.session.commit()

    message = 'successfully deleted a {} activity'.format(type_)
    return json.dumps({'status':'OK', 'message':message})

@main.route('/save-activity', methods=['POST'])
@login_required
def saveActivity():
    user_id = current_user.id
    type_ = request.json['type']
    
    user = User.query.filter_by(id=user_id).first()

    if (type_ == 'book'):
        book_id = request.json['book_id']
        book_href = request.json['href']
        book_author = request.json['book_author']
        book_title = request.json['book_title']
        # book_publisher = request.json['book_publisher']
        # book_average_rating = request.json['book_average_rating']
        # book_total_rating = request.json['book_total_rating']
        book_rating_elem = request.json['book_rating_elem']
        book_img_src = request.json['book_img_src']
        book_img_src_small = request.json['book_img_src_small']

        new_book = Book(book_id=book_id, book_author=book_author, book_title=book_title,
                        book_rating_elem=book_rating_elem, book_img_src=book_img_src,
                        book_img_src_small=book_img_src_small, href=book_href, users=user)
        
        user.activities.append(new_book)
        db.session.add(new_book)
        db.session.commit()
    
    elif (type_ == 'movie'):
        movie_id = request.json['movie_id']
        movie_href = request.json['href']
        movie_title = request.json['movie_title']
        movie_release_date = request.json['movie_release_date']
        movie_rating = request.json['movie_rating']
        movie_img_src = request.json['movie_img_src']

        new_movie = Movie(movie_id=movie_id, movie_title=movie_title, href=movie_href,
                        movie_release_date=movie_release_date, movie_rating=movie_rating,
                        movie_img_src=movie_img_src, users=user)

        user.activities.append(new_movie)
        db.session.add(new_movie)
        db.session.commit()
    
    message = 'successfully added a {} activity'.format(type_)
    return json.dumps({'status':'OK', 'message':message})

@main.route("/static/<path:filename>")
def staticfiles(filename):
    return send_from_directory(app.config["STATIC_FOLDER"], filename)

