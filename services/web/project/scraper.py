import requests
from datetime import datetime
from bs4 import BeautifulSoup
from flask import Markup
from flask_login import current_user
from .models import *
from . import db

def bookScrapper():
    books = []

    URL = 'https://www.goodreads.com/list/popular_lists'
    page = requests.get(URL)
    soup = BeautifulSoup(page.content, 'html.parser')
    results = soup.find(id='topRow')
    links = results.find_all('a', class_='listTitle')

    # get the top list (most popular list)
    URL2 = 'https://www.goodreads.com' + links[0]['href']
    page2 = requests.get(URL2)
    soup = BeautifulSoup(page2.content, 'html.parser')
    results = soup.find(id='all_votes')
    tr_list = results.find_all('tr') if results else []

    # print(tr_list[0])
    for tr in tr_list:
        book_id = tr.find('input', {'id': 'book_id'}).get('value')
        img_elem = tr.find('img', class_='bookCover')
        title_elem = tr.find('a', class_='bookTitle')
        rating_elem = Markup(tr.find('span', class_='greyText smallText uitext'))
        author_elem = tr.find('a', class_='authorName')
        href = 'https://www.goodreads.com' + title_elem['href']

        split = img_elem['src'].split('/')
        convert1 = split[-2].replace('i','l')
        convert2 = split[-1].split('.')
        convert2.pop(1)
        convert2 = '.'.join(convert2)
        split[-2] = convert1
        split[-1] = convert2
        # rejoin
        img_src = '/'.join(split)

        

        a_book = {"book_id": book_id,
                  "img": img_src,
                  "img_small": img_elem['src'],
                  "title": title_elem.text,
                  "href": href,
                  "ratingElem": rating_elem,
                  "author": author_elem.text}
                #   "saved": saved}
        books.append(a_book)
    # print(books[0])
    return books

def getMovies():
    movies = []

    API_KEY = "cf2cf097b666e6ab18b62a58db2ff0ef"
    API_URL = "https://api.themoviedb.org/3/discover/movie?api_key="+API_KEY+"&language=en-US&sort_by=popularity.desc&include_video=true&page="
    IMAGE_PATH = "https://image.tmdb.org/t/p/w500"

    resp = requests.get(API_URL)
    movie_data = resp.json()
    for movie in movie_data['results']:
        img_src = IMAGE_PATH + movie['poster_path']
        if (movie['release_date']):
            release_date_f1 = datetime.strptime(movie['release_date'],'%Y-%m-%d')
            release_date_f2 = release_date_f1.strftime('%b %d, %Y')
        else:
            release_date_f2 = ''
        ratings = str(int(float(movie['vote_average'])*10))
        a_movie = {"img": img_src,
                   "title": movie['original_title'],
                   "id": movie['id'],
                   "releaseDate": release_date_f2,
                   "ratings": ratings}
        movies.append(a_movie)
    return movies
    print(movie_data['results'][0])