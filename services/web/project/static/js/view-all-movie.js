$(document).ready(function() {
    const API_KEY = "249f222afb1002186f4d88b2b5418b55";

    const API_SEARCH = `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&language=en-US&query=`;

    const IMAGE_PATH = "https://image.tmdb.org/t/p/w500";

    let page = "1";

    const API_URL = `https://api.themoviedb.org/3/discover/movie?api_key=${API_KEY}&language=en-US&sort_by=popularity.desc&include_video=true&page=`;

    const mainContent = document.getElementById("movie-content");

    // search form elements
    const form = document.getElementById("search-form");
    const search = document.getElementById("search");

    // pagination elements
    const pageLinks = document.querySelectorAll(".page-link");

    // initially get the most popular movies list's first page
    getMovies(API_URL + page);

    // previous and next page
    pageLinks.forEach((pageLink) => {
        pageLink.addEventListener("click", () => {
            if (pageLink.id === "next") {
                page++;
                getMovies(API_URL + page);
            }
            if (pageLink.id === "previous" && page > 1) {
                page--;
                getMovies(API_URL + page);
            }
        });
    });

    // search for a movie
    form.addEventListener("submit", (e) => {
        e.preventDefault();
        const query = search.value;
        console.log(query)
        if (query) {
            getMovies(API_SEARCH + query);
        } else {
            if (query == "") {
                console.log("hahaha")
                getMovies(API_URL + page);
            }
        }
    });

    async function getMovies(url) {
        $('body').toggleClass('loading');
        const resp = await fetch(url);
        const respData = await resp.json();

        const data = { "data": respData.results }

        $.ajax({
                url: '/check-saved-activity',
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify(data),
                success: function(resp) {
                    console.log(resp.status);
                    showMovies(resp.movies);
                    $('body').toggleClass('loading');
                },
                error: function(err) {
                    console.log(err);
                }
            })
            // console.log(JSON.stringify(respData.results))

        // showMovies(respData.results);
        // $('body').toggleClass('loading');
    }

    function removeMovieFromDB(movie) {
        const movie_id = movie.id
        const data = {
            type: "movie",
            movie_id: movie_id
        }
        $.ajax({
            url: '/delete-activity',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function(resp) {
                console.log(resp);
            },
            error: function(err) {
                console.log(err);
            }
        })
    }

    function saveMovieToDB(movie) {
        const movie_id = movie.id
        const data = {
            type: "movie",
            href: "https://www.themoviedb.org/movie/" + movie_id,
            movie_id: movie_id,
            movie_title: movie.title,
            movie_release_date: movie.release_date,
            movie_rating: movie.vote_average,
            movie_img_src: "https://image.tmdb.org/t/p/w500" + movie.poster_path
        }
        $.ajax({
            url: '/save-activity',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function(resp) {
                console.log(resp);
            },
            error: function(err) {
                console.log(err);
            }
        })
    }

    function showMovies(movies) {
        mainContent.innerHTML = "";
        // console.log(movies)
        movies.forEach((movie) => {
            const movieId = movie.id;
            const movieTitle = movie.title;
            const moviePoster = movie.poster_path;
            const movieVote = movie.vote_average;
            const movieElm = document.createElement("div");
            const movieHref = "https://www.themoviedb.org/movie/" + movieId
            movieElm.classList.add(
                "col-xs-12",
                "col-sm-6",
                "col-md-4",
                "col-lg-3",
                "p-0"
            );
            movieElm.innerHTML = `
            <div class="movie-card">
                <div class="movie-img-content">
                    <div class="content-overlay"></div>
                    <img
                        class="img-fluid movie-img"
                        src="${IMAGE_PATH}${moviePoster}"
                        onError="this.onerror=null;this.src='https://i.ebayimg.com/images/g/1EMAAMXQdGJR2-n3/s-l1600.jpg';"
                        alt="Sorry, something went wrong"
                    />
                    <div class="content-details fadeIn-bottom">
                        <h3 class="content-title" id="content-title-${movieId}">save movie</h3>
                        <p class="content-text" class="content-text" id="content-text-${movieId}">Click the heart to save this movie</p>
                        <a id="icon-${movieId}" class="heart-icon" title="Save Movie">
                            <i class="fa fa-heart-o"></i>
                        </a>
                    </div>
                </div>
                <div class="movie-description p-3 d-flex justify-content-between align-items-center">
                    <a href=${movieHref} target="_blank">    
                        <h3 class="movie-title">${movieTitle}</h3>
                    </a>
                    <h3 class="movie-vote">${movieVote}</h3>
                </div>
            </div>`;
            mainContent.appendChild(movieElm);

            if (movie.saved == '1') {
                $("#icon-" + movieId).html('<i class="fa fa-heart" aria-hidden="true"></i>');
                $("#icon-" + movieId).addClass("liked");
                $("#content-title-" + movieId).text("movie saved")
                $("#content-text-" + movieId).text("Click the heart to unsave this movie")
            }

            $("#icon-" + movieId).click(function() {
                if ($("#icon-" + movieId).hasClass("liked")) {
                    $("#icon-" + movieId).html('<i class="fa fa-heart-o" aria-hidden="true"></i>');
                    $("#icon-" + movieId).removeClass("liked");
                    removeMovieFromDB(movie)
                    $("#content-title-" + movieId).text("save movie")
                    $("#content-text-" + movieId).text("Click the heart to save this movie")
                } else {
                    $("#icon-" + movieId).html('<i class="fa fa-heart" aria-hidden="true"></i>');
                    $("#icon-" + movieId).addClass("liked");
                    saveMovieToDB(movie)
                    $("#content-title-" + movieId).text("movie saved")
                    $("#content-text-" + movieId).text("Click the heart to unsave this movie")
                }
            });
        });
    }
});