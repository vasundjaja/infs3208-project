from flask.cli import FlaskGroup
from werkzeug.security import generate_password_hash

from project.models import User
from project import app, db


cli = FlaskGroup(app)


@cli.command("create_db")
def create_db():
    db.drop_all()
    db.create_all()
    db.session.commit()

@cli.command("seed_db")
def seed_db():
    hashed_password = generate_password_hash('admin', method='sha256')
    user_1 = User(email="vasundjaja@gmail.com", firstname="Vincentius Aditya",
                  lastname="Sundjaja", password=hashed_password)
    user_2 = User(email="test@gmail.com", firstname="Number 1",
                  lastname="Test", password=hashed_password)
    user_3 = User(email="test2@gmail.com", firstname="Number 2",
                  lastname="Test", password=hashed_password)
    
    db.session.add(user_1)
    db.session.add(user_2)
    db.session.add(user_3)
    db.session.commit()


if __name__ == "__main__":
    cli()