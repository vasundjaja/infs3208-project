# INFS3208-S4561009-PROJECT

This is a Cloud Computing course project. I created a web application called Leisure Time.

This application is mainly created with Python and the background processes are handled using Flask, Nginx, Gunicorn, and a Postgresql database.

## Installation

Clone this repo:
```bash
$ git clone https://gitlab.com/vasundjaja/infs3208-project.git
$ cd infs3208-project/
```
to be save, change the permission for entrypoint.sh
```bash
$ cd services/web/
$ chmod +x entrypoint.sh
```

Use docker-compose to spin up the containers.
Make sure you do the commands in the root folder

### For development:
```bash
$ docker-compose up -d --build
$ docker-compose exec web python manage.py create_db
```
don't forget to stop and remove it:
```bash
$ docker-compose down -v
```
Open your browser to [http://localhost:5000](http://localhost:5000) to view the app

#### Error checking:
```bash
$ docker logs -f <container_id>
$ docker logs --since 30s -f infs3208-s4561009-project_web_1
```

### For production:
#### use the --scale flag to scale up the web application so that nginx can automatically round-robin requests between the 2 containers
```bash
$ docker-compose -f docker-compose.prod.yml up -d --build --scale web=2
$ docker-compose -f docker-compose.prod.yml exec web python manage.py create_db
```
don't forget to stop and remove it:
```bash
$ docker-compose -f docker-compose.prod.yml down -v
```
Open your browser to [http://localhost:1337](http://localhost:1337) to view the app
#### Error checking:
```bash
$ docker-compose -f docker-compose.prod.yml logs -f
```

### Live Preview
I uploaded this project in a Google Cloud Platform VM instance. As long as the instance is on, you can try to access it here:
[http://34.68.48.83:1337](http://34.68.48.83:1337)

## Comments or Advices
If you see any problem or improvements, you can contact me at:
[s4561009@student.uq.edu.au](mailto:s4561009@student.uq.edu.au)

## References
- https://medium.com/@itaymelamed/building-a-python-scalable-flask-application-using-docker-compose-and-nginx-load-balancer-10b650e0a1b5
#### HTML CSS
- https://codepen.io/Ihor_Sukhorada/pen/LBwRvv/
- https://codepen.io/sarahhepworth/pen/MWKbNjq?editors=1100
- https://www.codeply.com/go/EIOtI7nkP8/bootstrap-carousel-with-multiple-cards
- https://codepen.io/leandroamato/pen/jOWqrGe
- https://codepen.io/emzarts/pen/JKxJdK?editors=1100
- https://codepen.io/ArnaudBalland/pen/vGZKLr
- https://codepen.io/bneupane/pen/KMKzZZ
- https://codepen.io/fatihsamur/pen/OJNQwYw
- https://codepen.io/maksdamir/pen/pADru